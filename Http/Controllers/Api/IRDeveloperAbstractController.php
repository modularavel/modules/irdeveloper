<?php
declare(strict_types=1);

namespace Modules\IRDeveloper\Http\Controllers\Api;

use Modules\IRCore\Http\Controllers\Api\CoreAbstractController;

abstract class IRDeveloperAbstractController extends CoreAbstractController
{
}
