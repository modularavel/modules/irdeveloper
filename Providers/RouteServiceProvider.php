<?php

namespace Modules\IRDeveloper\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Modules\IRCore\Providers\AbstractRouteServiceProvider;

class RouteServiceProvider extends AbstractRouteServiceProvider
{
    protected string $moduleName = 'IRDeveloper';
}
