<?php
declare(strict_types=1);

namespace Modules\IRDeveloper\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\IRCore\Providers\AbstractModuleServiceProvider;

class IRDeveloperServiceProvider extends AbstractModuleServiceProvider
{
    protected string $moduleName = 'IRDeveloper';
    protected string $moduleNameLower = 'irdeveloper';

    public function register(): ServiceProvider
    {
        return $this->app->register(RouteServiceProvider::class);
    }
}
